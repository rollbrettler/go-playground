package main

import (
	"fmt"
	"os"
	"runtime"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"

	"github.com/google/go-github/github"
)

var token string
var workers = runtime.NumCPU()

func main() {
	if token = os.Getenv("TOKEN"); token == "" {
		fmt.Printf("Please specify a TOKEN\n")
		os.Exit(1)
	}

	runtime.GOMAXPROCS(runtime.NumCPU())

	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(oauth2.NoContext, ts)

	client := github.NewClient(tc)
	ctx := context.Background()

	gists, request, _ := client.Gists.List(ctx, "", &github.GistListOptions{})

	fmt.Println("mirror_directory: ~/gist-mirror")
	fmt.Println("pre_run_command: echo 'pre_run_command'")
	fmt.Println("post_run_command: echo 'post_run_command'")
	fmt.Println("sources:")
	printGistInfos(gists)
	for i := 2; i <= request.LastPage; i++ {
		gists, _, _ := client.Gists.List(ctx, "", &github.GistListOptions{
			ListOptions: github.ListOptions{Page: i},
		})
		printGistInfos(gists)
	}
}

func printGistInfos(gists []*github.Gist) {
	for _, gist := range gists {
		if gist.Description != nil {
			if *gist.Description != "" {
				fmt.Printf("  - sources: %v\n    path: %v.git # %v\n    adapter: git\n", *gist.GitPullURL, *gist.ID, *gist.Description)
			} else {
				fmt.Printf("  - sources: %v\n    path: %v.git\n    adapter: git\n", *gist.GitPullURL, *gist.ID)
			}
		} else {
			fmt.Printf("  - sources: %v\n    path: %v.git\n    adapter: git\n", *gist.GitPullURL, *gist.ID)
		}
	}
}
