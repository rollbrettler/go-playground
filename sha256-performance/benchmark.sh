go build sha256-performance.go
du -h "$1"
time ./sha256-performance "$1"
if [ $(uname -s) = "Darwin" ]
then
  time shasum -a 256 "$1"
else
  time sha256sum "$1"
fi
