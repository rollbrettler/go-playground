package main

import (
	"fmt"
	"image"
	_ "image/png"
	"os"

	"github.com/muesli/smartcrop"
)

func main() {
	f, _ := os.Open("test.png")
	img, _, _ := image.Decode(f)

	analyzer := smartcrop.NewAnalyzer()
	topCrop, _ := analyzer.FindBestCrop(img, 250, 250)

	fmt.Printf("%vx%v+%v+%v", topCrop.Max.X-topCrop.Min.X, topCrop.Max.Y-topCrop.Min.Y, topCrop.Min.X, topCrop.Min.Y)
}
